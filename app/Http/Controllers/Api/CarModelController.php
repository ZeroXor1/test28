<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CarModel;
use Exception;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

class CarModelController extends Controller
{
    /**
     * @OA\Get(
     *    path="/car-models",
     *    operationId="car-models-index",
     *    tags={"Модели автомобилей"},
     *    summary="Получение списка моделей автомобилей",
     *    description="Получение списка моделей автомобилей",
     *    @OA\Parameter(name="limit", in="query", description="Лимит", required=false,
     *        @OA\Schema(type="integer")
     *    ),
     *    @OA\Parameter(name="page", in="query", description="Номер страницы", required=false,
     *        @OA\Schema(type="integer")
     *    ),
     *    @OA\Parameter(name="order", in="query", description="Порядок сортировки 'asc' или 'desc'", required=false,
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Response(
     *         response=200, description="Success",
     *         @OA\JsonContent(
     *            @OA\Property(property="status", type="integer", example="200"),
     *            @OA\Property(property="data", type="object")
     *         )
     *    )
     *  )
     */
    public function index(Request $request)
    {
        try {
            $limit = $request->limit ?: 10;
            $order = $request->order == 'asc' ? 'asc' : 'desc';

            $carBrands = CarModel::orderBy('updated_at', $order)
                ->select('id', 'name', 'car_brand_id')
                ->with('carBrand')
                ->paginate($limit);

            return response()->json(['status' => 200, 'data' => $carBrands]);
        } catch (Exception $e) {
            return response()->json(['status' => 400, 'message' => $e->getMessage()]);
        }
    }
}
