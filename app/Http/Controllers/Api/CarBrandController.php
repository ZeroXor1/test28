<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CarBrand;
use Exception;
use Illuminate\Http\Request;

class CarBrandController extends Controller
{
    /**
     * @OA\Get(
     *    path="/car-brands",
     *    operationId="car-brands-index",
     *    tags={"Марки автомобилей"},
     *    summary="Получение списка марок автомобилей",
     *    description="Получение списка марок автомобилей",
     *    @OA\Parameter(name="limit", in="query", description="Лимит", required=false,
     *        @OA\Schema(type="integer")
     *    ),
     *    @OA\Parameter(name="page", in="query", description="Номер страницы", required=false,
     *        @OA\Schema(type="integer")
     *    ),
     *    @OA\Parameter(name="order", in="query", description="Порядок сортировки 'asc' или 'desc'", required=false,
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Response(
     *         response=200, description="Success",
     *         @OA\JsonContent(
     *            @OA\Property(property="status", type="integer", example="200"),
     *            @OA\Property(property="data", type="object")
     *         )
     *    )
     *  )
     */
    public function index(Request $request)
    {
        try {
            $limit = $request->limit ?: 10;
            $order = $request->order == 'asc' ? 'asc' : 'desc';

            $carBrands = CarBrand::orderBy('updated_at', $order)
                ->select('id', 'name')
                ->paginate($limit);

            return response()->json(['status' => 200, 'data' => $carBrands]);
        } catch (Exception $e) {
            return response()->json(['status' => 400, 'message' => $e->getMessage()]);
        }
    }
}
