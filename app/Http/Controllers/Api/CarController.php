<?php

namespace App\Http\Controllers\Api;

use App\Models\Car;
use Exception;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use OpenApi\Annotations as OA;

class CarController extends Controller
{
    /**
     * @OA\Get(
     *    path="/cars",
     *    operationId="cars-index",
     *    tags={"Автомобили"},
     *    summary="Получение списка автомобилей",
     *    description="Получение списка автомобилей",
     *    @OA\Parameter(name="limit", in="query", description="Лимит", required=false,
     *        @OA\Schema(type="integer")
     *    ),
     *    @OA\Parameter(name="page", in="query", description="Номер страницы", required=false,
     *        @OA\Schema(type="integer")
     *    ),
     *    @OA\Parameter(name="order", in="query", description="Порядок сортировки 'asc' или 'desc'", required=false,
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Parameter(name="user", in="query", description="ID пользователя", required=false,
     *        @OA\Schema(type="string")
     *    ),
     *    @OA\Response(
     *         response=200, description="Success",
     *         @OA\JsonContent(
     *            @OA\Property(property="status", type="integer", example="200"),
     *            @OA\Property(property="data", type="object")
     *       )
     *    )
     *  )
     */
    public function index(Request $request) : JsonResponse
    {
        try {
            $limit = $request->limit ?: 10;
            $order = $request->order == 'asc' ? 'asc' : 'desc';
            $user = $request->user ?: null;

            $cars = Car::query()
                ->with('carModel')
                ->with('carBrand')
                ->with('user')
                ->select('id', 'car_model_id', 'user_id', 'manufactured_year', 'mileage', 'color')
                ->when($user, function ($query, $user) {
                    return $query->where('user_id', $user);
                })
                ->orderBy('updated_at', $order)
                ->paginate($limit);

            return response()->json(['status' => 200, 'data' => $cars]);
        } catch (Exception $e) {
            return response()->json(['status' => 400, 'message' => $e->getMessage()]);
        }
    }

    /**
     * @OA\Post(
     *      path="/cars",
     *      operationId="cars-store",
     *      tags={"Автомобили"},
     *      summary="Сохранить новую запись автомобиля в БД",
     *      description="Сохранить новую запись автомобиля в БД",
     *      @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *            required={"car_model_id", "user_id"},
     *            @OA\Property(property="car_model_id", type="integer", format="integer", example="1"),
     *            @OA\Property(property="user_id", type="integer", format="integer", example="1"),
     *            @OA\Property(property="manufactured_year", type="integer", format="integer", example="2020"),
     *            @OA\Property(property="mileage", type="integer", format="integer", example="500"),
     *            @OA\Property(property="color", type="string", format="string", example="черный"),
     *         ),
     *      ),
     *     @OA\Response(
     *          response=200, description="Success",
     *          @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example=""),
     *             @OA\Property(property="data",type="object")
     *        )
     *     )
     *  )
     */
    public function store(Request $request) : JsonResponse
    {
        try {
            DB::beginTransaction();

            $car = Car::create($request->only('car_model_id', 'user_id', 'manufactured_year', 'mileage', 'color'));
            DB::commit();

            return response()->json(['status' => 201, 'data' => $car]);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['status' => 400, 'message' => $e->getMessage()]);
        }
    }

    /**
     * @OA\Get(
     *    path="/cars/{id}",
     *    operationId="cars-show",
     *    tags={"Автомобили"},
     *    summary="Получить данные об автомобиле",
     *    description="Получить данные об автомобиле",
     *    @OA\Parameter(name="id", in="path", description="Id автомобиля", required=true,
     *        @OA\Schema(type="integer")
     *    ),
     *     @OA\Response(
     *          response=200,
     *          description="Success",
     *          @OA\JsonContent(
     *          @OA\Property(property="status", type="integer", example="200"),
     *          @OA\Property(property="data",type="object")
     *           ),
     *        )
     *     )
     *  )
     */
    public function show(Request $request, int $id) : JsonResponse
    {
        try {
            $user = $request->user ?: null;
            $car = Car::findOrFail($id)
                ->where('id', $id)
                ->with('carModel')
                ->with('carBrand')
                ->with('user')
                ->when($user, function ($query, $user) {
                    return $query->where('user_id', $user);
                })->get();
            return response()->json(['status' => 200, 'data' => $car]);
        } catch (Exception $e) {
            return response()->json(['status' => 400, 'message' => $e->getMessage()]);
        }
    }

    /**
     * @OA\Put(
     *     path="/cars/{id}",
     *     operationId="cars-update",
     *     tags={"Автомобили"},
     *     summary="Обновление записи автомобиля в БД",
     *     description="Обновление записи автомобиля в БД",
     *     @OA\Parameter(name="id", in="path", description="Id автомобиля", required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *        required=true,
     *        @OA\JsonContent(
     *           required={"car_model_id", "user_id"},
     *           @OA\Property(property="car_model_id", type="integer", format="integer", example="1"),
     *           @OA\Property(property="user_id", type="integer", format="integer", example="1"),
     *           @OA\Property(property="manufactured_year", type="integer", format="integer", example="2020"),
     *           @OA\Property(property="mileage", type="integer", format="integer", example="500"),
     *           @OA\Property(property="color", type="string", format="string", example="черный"),
     *        ),
     *     ),
     *     @OA\Response(
     *          response=200, description="Success",
     *          @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="200"),
     *             @OA\Property(property="data",type="object")
     *          )
     *       )
     *  )
     */
    public function update(Request $request, int $id) : JsonResponse
    {
        try {
            DB::beginTransaction();

            $car = Car::updateOrCreate(['id' => $id],
                $request->only('car_model_id', 'user_id', 'manufactured_year', 'mileage', 'color'));
            DB::commit();
            return response()->json(['status' => 200, 'data' => $car]);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['status' => 400, 'message' => $e->getMessage()]);
        }
    }

    /**
     * @OA\Delete(
     *    path="/cars/{id}",
     *    operationId="cars-destroy",
     *    tags={"Автомобили"},
     *    summary="Удалить запись автомобиля",
     *    description="Удалить запись автомобиля",
     *    @OA\Parameter(name="id", in="path", description="Id автомобиля", required=true,
     *        @OA\Schema(type="integer")
     *    ),
     *    @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *         @OA\Property(property="status", type="integer", example="200"),
     *         @OA\Property(property="data",type="object")
     *          ),
     *       )
     *      )
     *  )
     */
    public function destroy(int $id) : JsonResponse
    {
        try {
            Car::findOrFail($id)->delete();

            return response()->json(['status' => 200, 'data' => []]);
        } catch (Exception $e) {
            return response()->json(['status' => 400, 'message' => $e->getMessage()]);
        }
    }
}
