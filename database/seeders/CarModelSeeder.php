<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CarModelSeeder extends Seeder
{
    private $carModels = [
        [
            'name'  => '2105',
            'car_brand_id' => 1
        ],
        [
            'name'  => '2106',
            'car_brand_id' => 1
        ],
        [
            'name'  => '2107',
            'car_brand_id' => 1
        ],
        [
            'name'  => '2108',
            'car_brand_id' => 1
        ],
        [
            'name'  => 'Нива 4x4',
            'car_brand_id' => 1
        ],
        [
            'name'  => 'Vesta',
            'car_brand_id' => 2
        ],
        [
            'name'  => 'Granta',
            'car_brand_id' => 2
        ],
        [
            'name'  => 'Largus',
            'car_brand_id' => 2
        ],
        [
            'name'  => '3',
            'car_brand_id' => 3
        ],
        [
            'name'  => '3е',
            'car_brand_id' => 3
        ],
        [
            'name'  => '6',
            'car_brand_id' => 3
        ],
    ];

    /**
     * Seed car_models table
     *
     * @return void
     */
    public function run()
    {
        DB::table('car_models')->insert($this->carModels);
    }
}
