<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CarBrandSeeder extends Seeder
{
    private $carBrands = [
        [
            'id' => 1,
            'name'  => 'ВАЗ',
        ],
        [
            'id' => 2,
            'name'  => 'LADA',
        ],
        [
            'id' => 3,
            'name'  => 'Москвич',
        ],
    ];

    /**
     * Seed car_brands table
     *
     * @return void
     */
    public function run()
    {
        DB::table('car_brands')->insert($this->carBrands);
    }
}
