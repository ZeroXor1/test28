<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CarSeeder extends Seeder
{
    /**
     * Seed cars table
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Car::factory(50)->create();
    }
}
