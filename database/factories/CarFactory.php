<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Ramsey\Uuid\Type\Integer;
use App\Models\CarModel;
use App\Models\User;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Car>
 */
class CarFactory extends Factory
{
    private $carColors = ['черный', 'белый', 'серебристый', 'металлик', 'красный', 'синий', 'мокрый асфальт'];
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'car_model_id' => CarModel::all()->random()->id,
            'user_id' => User::all()->random()->id,
            'manufactured_year' => $this->faker->numberBetween(1990, date('Y')),
            'mileage' => $this->faker->numberBetween(0, 100000),
            'color' => $this->faker->randomElement($this->carColors)
        ];
    }
}
