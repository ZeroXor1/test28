# Тестовое задание по разработке REST API на фреймворке Laravel 9

<p align="center">
<a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="300"></a>
<a href="#" target="_blank"><img src="https://1000logos.net/wp-content/uploads/2020/08/Nginx-Symbol-500x313.jpg" width="180" /></a>
<a href="#" target="_blank"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRDZBC6IlFaO5vOi_jq8RLvR7dX970fNfjNlt9BI4aWPPacKm_uORC4lvAmy1sz4MTTXjo&usqp=CAU" width="160" /></a>
<a href="#" target="_blank"><img src="https://mariadb.com/wp-content/uploads/2019/11/mariadb-logo-vert_blue-transparent-300x245.png" width="140" /></a>
<a href="#" target="_blank"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxYN4XIJfWMZJu7r-mIYY1g-Fk6zsEir37be49wRT9uSqoZ-Oef-6zwCedoR97lD9a_Ic&usqp=CAU" width="170" /></a>
<a href="#" target="_blank"><img src="https://xano.ghost.io/content/images/2022/09/openAPI-specification-the-definitive-guide.png" width="300" /></a>
</p>

<!--
<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>
-->

> **Описана установка и работа в Ubuntu 22.04**

---

## УСТАНОВКА

---

### Общая установка проекта

>**ВНИМАНИЕ!**\
> Для установки проекта понадобится composer.\
> Для сборки окружения понадобятся docker и docker-compose

Скачайте проект с Gitlab

```bash
git clone https://gitlab.com/ZeroXor1/test28.git test28
```

Перейдите в корневой каталог проекта
```bash
cd test28
```

>**ВНИМАНИЕ!**\
> Все дальнейшие команды выполняются из корневого каталога проекта

Установите проект с помощью composer
```bash
composer install
```
Создайте в корне проекта файл .env и скопируйте в него содержимое файла .env.example

---

### Установка на локальный сервер

---

> Здесь я не буду описывать настройки серверов. прописыванию локальных хостов и всего такого прочего.
> И опишу запуск проекта под докером, мне эта технология нравится все больше и больше.

---

### Установка с помощью docker-compose

---

Соберите окружение (в нашем случае будет собираться только PHP, остальное окружение мы возьмем из заранее собранных образов)

```bash
docker-compose build
```

Скачайте и установите готовые образы (nginx, mysql, phpmyadmin) - и запустите окружение

```bash
docker-compose up -d
```

Для остановки окружения выполните следующую команду:
```bash
docker-compose down
```

---

### Проверка

---

Индексная страница - **http://localhost/**

PhpMyAdmin - **http://localhost:8000**
> Логин - user \
> Пароль - password

---

### Создание таблиц в базе данных и наполнение их тестовыми данными

---

Создайте таблицу. в которой будут храниться данные о миграциях

```bash
docker-compose exec php php artisan migrate:install
```

>**ВНИМАНИЕ!**\
> При появлении ошибки **[2002] Connection refused** выполните команду:
> ```bash
> docker-compose ps
> ```
> Посмотрите, как у Вас называется образ БД. У меня это **test28_db_1**. \
> Если у Вас он называется иначе, пропишите это имя в файле .env в переменной DB_HOST.

Создайте таблицы (примените миграции)

```bash
docker-compose exec php php artisan migrate
```

Заполните таблицы тестовыми данными (примените сиды)

>**ВНИМАНИЕ!**\
> Если хотите снова загрузить тестовые данные, то сначала выполните команду:
> ```bash
> docker-compose exec php php artisan migrate:fresh
> ```
> Это очистит таблицы и не возникнет конфликтов с попытками дублирования id в базе данных

```bash
docker-compose exec php php artisan db:seed
```

---

### УСТАНОВКА ПРОЕКТА ЗАВЕРШЕНА !!!

---

---

## ДОКУМЕНТИРОВАНИЕ

---

В проекте используется документация OpenAPI. Подключим ее:

```bash
docker-compose exec php php artisan vendor:publish --provider "L5Swagger\L5SwaggerServiceProvider"
```

Для ее просмотра откройте страницу:

>http://localhost/api/documentation

Там же можно делать запросы в API, не используя никаких сторонних приложений.

Ведение документации требует определенных знаний. Получить их можно **[здесь](https://learn.openapis.org/)**.

При внесении изменений в описание документации выполните команду:

```bash
docker-compose exec php php artisan l5-swagger:generate
```
чтобы применить эти изменения (сгенерировать новую документацию)

---

## ИСПОЛЬЗОВАНИЕ

---

## ЛИЦЕНЗИЯ

---

Фреймворк Laravel — это программное обеспечение с открытым исходным кодом, лицензированное по [лицензии MIT](https://opensource.org/licenses/MIT).
